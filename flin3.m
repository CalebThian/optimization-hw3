function [fval]=flin3(a,b,c)
    global x y
    % f(a,b,c)=sum((ax_i^2+bx_i+c-yi)^2)
    fval=0;
    for i=1:length(x)
        fval=fval+(a*x(i)^2+b*x(i)+c-y(i))^2;
    end
end