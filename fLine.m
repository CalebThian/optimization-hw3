function z = fLine(lambda) % F in the search direction V
global X FUNC S
z = feval(FUNC,X(1)+lambda*S(1),X(2)+lambda*S(2));