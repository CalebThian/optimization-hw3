clear all
global FUNC X S
FUNC=@ftry;
eps=0.0000000001;
k=1;
j=k;
Y=[1;0];
X=[1;0];
xc=[X];
n=100;
%x_j=y_k
xc(j*2-1)=Y(1);
xc(j*2)=Y(2);
X(1)=xc(j*2-1);
X(2)=xc(j*2);

df=[4*X(1)+2*X(2)+1;2*X(1)+2*X(2)-1];
S=-df;

lamda=ones(100,1);
while(1)
    if(abs(S)<eps)
        break
    else
        [lamda(j),f,fv]=goldSearch(@fLine2,-10,10);
        X=X+lamda(j)*S;
        xc(j*2+1)=X(1);
        xc(j*2+2)=X(2);
        if(j<n)    
            df2=[4*X(1)+2*X(2)+1;2*X(1)+2*X(2)-1];
            beta=(df2'*df2)/(df'*df);
            S=-df2+beta*S;
            df=df2;
            j=j+1;
        else
            j=1;
            Y(1)=xc(2*n+1);
            Y(2)=xc(2*n+2);
            xc(1)=Y(1);
            xc(2)=Y(2);
            X(1)=Y(1);
            X(2)=Y(2);
            df=[4*X(1)+2*X(2)+1;2*X(1)+2*X(2)-1];
            S=-df;
        end
    end
end