x=[0.1,0.9,2.2,3,4.1,5.2,5.9,6.8,8.1,8.7,9.2,11,12.4,14.1,15.2,16.8,18.7,19.9,22];
y=[28.5,27,29,30.5,37.3,36.4,32.4,28.5,30,36.1,39,36,32,28,22,20,27,40,61];
%% Question
% In a linear model,goal:minimize f(m,b)=∑_(i=1)^19(mx_i+b-y_i )^2 
% f(m,b)=∑_(i=1)^19(m^2 (x_i)^2+ b^2+(y_i)^2+2mbx_i-2mx_i y_i-2by_i)
% f(m,b)=∑_(i=1)^19(m^2 (x_i)^2+2mbx_i+ b^2-2mx_i y_i-2by_i+(y_i)^2)
sum_of_x_sq = x*x';
sum_of_two_times_x = 2*x*ones(length(x),1);
sum_of_two_times_xy= 2*x*y';
sum_of_two_times_y = 2*y*ones(length(y),1);
sum_of_y_sq = y*y';
%% for linear regression
degree=1;
degree=degree+1;
X=ones(length(x),degree);
for i=1:length(x)
    for j=1:degree
        X(i,j)=x(i)^(j-1);
    end
end

a=inv(X'*X)*X'*y';

%% for quadratic regression
degree=2;
degree=degree+1;
X=ones(length(x),degree);
for i=1:length(x)
    for j=1:degree
        X(i,j)=x(i)^(j-1);
    end
end

a=inv(X'*X)*X'*y';