# -*- coding: utf-8 -*-
"""
Created on Mon May 18 22:23:13 2020

@author: caleb
"""


# -*- coding: utf-8 -*-
"""
Created on Mon May 18 18:45:39 2020

@author: caleb
"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns; sns.set()
from sklearn.preprocessing import PolynomialFeatures
from sklearn.pipeline import make_pipeline
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error,r2_score

x = np.array([0.1,0.9,2.2,3,4.1,5.2,5.9,6.8,8.1,8.7,9.2,11,12.4,14.1,15.2,16.8,18.7,19.9,22])
y = np.array([28.5,27,29,30.5,37.3,36.4,32.4,28.5,30,36.1,39,36,32,28,22,20,27,40,61])
lw=2
rmse=np.random.random(len(x)-1)
r2=np.random.random(len(x)-1)
ind = np.arange(len(x)-1)
ind=ind+1

for i in range(1,len(x)):
    poly_model=make_pipeline(PolynomialFeatures(i),LinearRegression())
    poly_model.fit(x[:,np.newaxis],y)
    xfit = np.linspace(0.1,22,int((22-0.1)*100))
    yfit = poly_model.predict(xfit[:,np.newaxis])
    plt.scatter(x,y)
    plt.plot(xfit,yfit)
    plt.title(label="degree %d" % i)
    plt.show()
    plt.xlabel("x");
    plt.ylabel("y");
    # Score calculation
    y_pre=poly_model.predict(x[:,np.newaxis])
    rmse[i-1]=np.sqrt(mean_squared_error(y,y_pre))
    r2[i-1]=r2_score(y,y_pre)
    
dict = {"degree":ind,"RMSE" : rmse, "R2 score": r2}
df=pd.DataFrame(dict)
print(df)
#df.to_csv(r'D:\Course\Junior2\Optimization_Design_Theories_and_Applications_To_Sports_Industry_and_Engineering\homework\hw3\optimization-hw3\Q3.csv',index=False,header=True)
