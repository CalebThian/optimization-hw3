function [fval]=ftry(x,y)
fval=2*x^2+2*x*y+y^2+x-y;
end