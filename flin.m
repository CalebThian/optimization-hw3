function [fval]=flin(m,b)
    global x y
    %f(m,b)=sum((mx_i+b-yi)^2)
    fval=0;
    for i=1:length(x)
        fval=fval+(m*x(i)+b-y(i))^2;
    end
end