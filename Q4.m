% modelfun = @(b,x)(b(1)+b(2)*exp(-b(3)*x));
% rng('default') % for reproducibility
% b = [1;3;2];
% x = exprnd(2,100,1);
% y = modelfun(b,x) + normrnd(0,0.1,100,1);
% opts = statset('nlinfit');
% opts.RobustWgtFun = 'bisquare';
% beta0 = [2;2;2];
% beta = nlinfit(x,y,modelfun,beta0,opts)

modelfun = @(c,x)c(1)+c(2)*x.*cos(c(3)*x)+c(3)*x.^1+c(4)*x.^2;
rng('default')
x=[0.1,0.9,2.2,3,4.1,5.2,5.9,6.8,8.1,8.7,9.2,11,12.4,14.1,15.2,16.8,18.7,19.9,22];
y=[28.5,27,29,30.5,37.3,36.4,32.4,28.5,30,36.1,39,36,32,28,22,20,27,40,61];
opts=statset('nlinfit');
opts.RobustWgtFun = 'bisquare';
coe0=[1;2;pi;4];
coe = nlinfit(x,y,modelfun,coe0,opts);

hold on;
xi=0:0.01:22;
yi=feval(modelfun,coe,xi);
h1=scatter(x,y);
plot(xi,yi);
xlabel("x");
ylabel("y");
title("Nonlinear Regression Model")
hold off;