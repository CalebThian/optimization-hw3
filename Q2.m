clear all
global x y FUNC X S
x=[0.1,0.9,2.2,3,4.1,5.2,5.9,6.8,8.1,8.7,9.2,11,12.4,14.1,15.2,16.8,18.7,19.9,22];
y=[28.5,27,29,30.5,37.3,36.4,32.4,28.5,30,36.1,39,36,32,28,22,20,27,40,61];
%% some common variables
sumx=0;%sum of x
sumxx=0;%sum of x^2
sumxxx=0;%sum of x^3
sumx4=0;%sum of x^4
sumy=0;%sum of y
sumxy=0;%sum of xy
sumxxy=0;%sum of xxy

for i=1:length(x)
    sumx=sumx+x(i);
    sumxx=sumxx+x(i)^2;
    sumxxx=sumxxx+x(i)^3;
    sumx4=sumx4+x(i)^4;
    sumy=sumy+y(i);
    sumxy=sumxy+x(i)*y(i);
    sumxxy=sumxxy+x(i)*x(i)*y(i);
end
%% Fletcher-Reeves Conjugate Gradient Method--Linear
% minimize: f(m,b)=sum((mx_i+b-yi)^2)
% Standard form of quadratic function:f(X)=0.5X'AX+b'X+C
% where A=[2xx' 2*sum_of_x;2*sum_of_x 38],b=[-2xy';-2*sum_of_y],C=y*y',X=[m;b]

FUNC = @flin;
%let X_1=[0;0]
eps=0.0001;
k=1;
j=k;
Y=[1;0];
X=[1;0];
xc=[X];
n=1000;
%x_j=y_k
xc(j*2-1)=Y(1);
xc(j*2)=Y(2);
X(1)=xc(j*2-1);
X(2)=xc(j*2);

df=[2*sumxx*X(1)+2*sumx*X(2)-2*sumxy;2*sumx*X(1)+38*X(2)-2*sumy];
S=-df;
sc=[S];
lamda=ones(n,1);
while(1)
    if((S(1)^2+S(2)^2)^(0.5)<eps)
        break
    else
        [lamda(j),f,fv]=goldSearch(@fLine,-10,10);
        X=X+lamda(j)*S;
        xc(j*2+1)=X(1);
        xc(j*2+2)=X(2);
        if(j<n)    
            df2=[2*sumxx*X(1)+2*sumx*X(2)-2*sumxy;2*sumx*X(1)+38*X(2)-2*sumy];
            beta=(df2'*df2)/(df'*df);
            S=-df2+beta*S;
            sc(j*2-1)=S(1);
            sc(j*2)=S(2);
            df=df2;
            j=j+1;
        else
            j=1;
            Y(1)=xc(2*n+1);
            Y(2)=xc(2*n+2);
            xc(1)=Y(1);
            xc(2)=Y(2);
            X(1)=Y(1);
            X(2)=Y(2);
            df=[2*sumxx*X(1)+2*sumx*X(2)-2*sumxy;2*sumx*X(1)+38*X(2)-2*sumy];
            S=-df;
        end
    end
end

fprintf("For the linear regression model, the argument m=%f,b=%f by Fletcher-Reeves Conjugate Gradient Method.\n",X(1),X(2));

%% Fletcher-Reeves Conjugate Gradient Method--Quadratic

% minimize: f(a,b,c)=sum((ax_i^2+bx_i+c-yi)^2)
FUNC = @flin3;

eps=0.1;
k=1;
j=k;
Y=[0;0;0];
X=[0;0;0];
xc=[X];
n=1000;
%x_j=y_k
xc(j*3-2)=Y(1);
xc(j*3-1)=Y(2);
xc(j*3)=Y(3);
X(1)=xc(j*3-2);
X(2)=xc(j*3-1);
X(3)=xc(j*3);

df=[2*sumx4*X(1)+2*sumxxx*X(2)+2*sumxx*X(3)-2*sumxxy;...
    2*sumxxx*X(1)+2*sumxx*X(2)+2*sumx*X(3)-2*sumxy;...
    2*sumxx*X(1)+2*sumx*X(2)+38*X(3)-2*sumy];
S=-df;
sc=[S];
lamda=ones(n,1);
while(1)
    if((S(1)^2+S(2)^2+S(3)^2)^(0.5)<eps)
        break
    else
        [lamda(j),f,fv]=goldSearch(@fLine3,-10,10);
        X=X+lamda(j)*S;
        xc(j*3+1)=X(1);
        xc(j*3+2)=X(2);
        xc(j*3+3)=X(3);
        if(j<n)    
            df2=[2*sumx4*X(1)+2*sumxxx*X(2)+2*sumxx*X(3)-2*sumxxy;...
                2*sumxxx*X(1)+2*sumxx*X(2)+2*sumx*X(3)-2*sumxy;...
                2*sumxx*X(1)+2*sumx*X(2)+38*X(3)-2*sumy];
            beta=(df2'*df2)/(df'*df);
            S=-df2+beta*S;
            sc(j*3-2)=S(1);
            sc(j*3-1)=S(2);
            sc(j*3)=S(3);
            df=df2;
            j=j+1;
        else
            j=1;
            Y(1)=xc(3*n+1);
            Y(2)=xc(3*n+2);
            Y(3)=xc(3*n+3);
            xc(1)=Y(1);
            xc(2)=Y(2);
            xc(3)=Y(3);
            X(1)=Y(1);
            X(2)=Y(2);
            X(3)=Y(3);
            df=[2*sumx4*X(1)+2*sumxxx*X(2)+2*sumxx*X(3)-2*sumxxy;...
                2*sumxxx*X(1)+2*sumxx*X(2)+2*sumx*X(3)-2*sumxy;...
                2*sumxx*X(1)+2*sumx*X(2)+38*X(3)-2*sumy];
            S=-df;
        end
    end
end

fprintf("For the quadratic regression model, the argument a=%f,b=%f and c=%f by Fletcher-Reeves Conjugate Gradient Method.\n",X(1),X(2),X(3));

%% Using DFP Method-Linear

FUNC = @flin;
eps=0.0001;
X=[1;0];
B=eye(2);
df=[2*sumxx*X(1)+2*sumx*X(2)-2*sumxy;2*sumx*X(1)+38*X(2)-2*sumy];
while(1)
    if((df(1)^2+df(2)^2)^(0.5)<eps)
        break;
    else
        S=-B*df;
        [lamda,f,fv]=goldSearch(@fLine,-10,10);
        X=X+lamda*S;
        df2=[2*sumxx*X(1)+2*sumx*X(2)-2*sumxy;2*sumx*X(1)+38*X(2)-2*sumy];
        if((df2(1)^2+df2(2)^2)^(0.5)<eps) 
            break; 
        end
        g=df2-df;
        M=lamda*S*S'/(S'*g);
        N=-(B*g)*(B*g)'/(g'*B*g);
        B=B+M+N;
        df=df2;
    end
end

fprintf("For the linear regression model, the argument m=%f,b=%f by DFP Method.\n",X(1),X(2));

%%  Using DFP Method-Quadratic
FUNC = @flin3;
eps=0.001;
X=[0;0;0];
B=eye(3);
df=[2*sumx4*X(1)+2*sumxxx*X(2)+2*sumxx*X(3)-2*sumxxy;...
    2*sumxxx*X(1)+2*sumxx*X(2)+2*sumx*X(3)-2*sumxy;...
    2*sumxx*X(1)+2*sumx*X(2)+38*X(3)-2*sumy];
while(1)
    if((df(1)^2+df(2)^2+df(3)^2)^(0.5)<eps)
        break;
    else
        S=-B*df;
        [lamda,f,fv]=goldSearch(@fLine3,-10,10);
        X=X+lamda*S;
        df2=[2*sumx4*X(1)+2*sumxxx*X(2)+2*sumxx*X(3)-2*sumxxy;...
             2*sumxxx*X(1)+2*sumxx*X(2)+2*sumx*X(3)-2*sumxy;...
             2*sumxx*X(1)+2*sumx*X(2)+38*X(3)-2*sumy];
        if((df2(1)^2+df2(2)^2+df2(3)^2)^(0.5)<eps) 
            break; 
        end
        g=df2-df;
        M=lamda*S*S'/(S'*g);
        N=-(B*g)*(B*g)'/(g'*B*g);
        B=B+M+N;
        df=df2;
    end
end

fprintf("For the quadratic regression model, the argument a=%f,b=%f and c=%f by DFP Method.\n",X(1),X(2),X(3));